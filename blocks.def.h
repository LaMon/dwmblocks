//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
 /*Icon*/ /*Command*/ /*Update Interval*/ /*Update Signal */
 {" 🐧 ", "$HOME/Doc/repo/dwmblocks/scripts/kernel", 360, 2},

 {" 🔺 ", "$HOME/Doc/repo/dwmblocks/scripts/upt", 60, 2},

 {" 💻 ", "$HOME/Doc/repo/dwmblocks/scripts/memory", 6, 1},

 {" 🔊 ", "$HOME/Doc/repo/dwmblocks/scripts/volume", 2, 10},

 {" 🌐 ", "$HOME/Doc/repo/dwmblocks/scripts/inet", 5, 0},
 
 {" 🕑 ", "$HOME/Doc/repo/dwmblocks/scripts/clock", 5, 0},

 {" 🔌 ", "$HOME/Doc/repo/dwmblocks/scripts/power", 30, 0},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim = '/';
